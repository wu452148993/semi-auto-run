export function setup(ctx) {
    const id = 'semi-auto-run';
    const title = 'SEMI Auto Run';

    const adjustedMaxHit = (player, enemy) => {
        let maxhit = enemy.getAttackMaxDamage(enemy.nextAttack);
        maxhit = enemy.applyDamageModifiers(player, maxhit);
        maxhit += enemy.modifiers.getFlatReflectDamage();
        return Math.ceil(maxhit);
    };

    const runFromCombat = (reason) => {
        game.combat.stop();
        notifyPlayer(game.attack, `[${title}] Ran from combat, ${reason}.`, 'danger');
        mod.api.SEMI.log(id, `Ran from combat, ${reason}.`);
    };

    ctx.patch(Enemy, 'queueNextAction').after((result) => {
        const player = game.combat.player;
        const enemy = game.combat.enemy;

        const attackDamage = adjustedMaxHit(player, enemy);

        // Max Hit
        if (attackDamage >= player.stats.maxHitpoints) {
            runFromCombat('next hit greater then max hitpoints');
            return;
        }

        // Current HP & No Auto Eat
        if (player.autoEatThreshold <= 0) {
            if (attackDamage >= player.hitpoints) {
                runFromCombat('next hit higher then current hitpoints');
                return;
            }
        }

        // Food
        const playerFood = player.food;
        if (player.food.currentSlot.quantity <= 0) {
            let hasFood = false;

            // In-Game Auto Food Swap Enabled
            if (game.modifiers.autoSwapFoodUnlocked > 0 && game.settings.enableAutoSwapFood) {
                hasFood |= player.food.slots.some(slot => slot.item !== game.emptyFoodItem);
            }

            if (!hasFood) {
                runFromCombat('no food remaining');
                return;
            }
        }
    });

    mod.api.SEMI.log(id, 'Successfully loaded!');
}